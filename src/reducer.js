import {Map} from 'immutable';
import * as CONST from './constants';

const actionMap = {
	[CONST.SET_STATE]: (state) => {

	}
};

/**
 * Reducer function mapping action objects to state transition functions
 *
 * @param {Map} state
 * @param {Object} action
 * @return {Map}
 */
export default function reducer(state=Map(), action) {
	// Return default state if action is undefined
	if ( ! Object.keys(actionMap).includes(action)) {
		return state;
	}
}