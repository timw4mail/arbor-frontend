/**
 * 'Global' constants
 */
export const SET_STATE = 'SET_STATE';
export const UPDATE = 'UPDATE';
export const DELETE = 'DELETE';