const webpack = require('webpack');

module.exports = {
	cache: true,
	debug: true,
	devServer: {
		contentBase: './build',
		hot: true
	},
	devtool: 'sourcemaps',
	entry: [
		'webpack/hot/only-dev-server',
		'./src/app.js',
	],
	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /(node_modules)/,
			loader: 'react-hot!babel-loader'
		}, {
			test: /\.css$/,
			loader: 'style-loader!css-loader!postcss-loader'
		}]
	},
	output: {
		path: `${__dirname}/build`,
		publicPath: '/',
		filename: 'bundle.js'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	],
	postcss: () => {

	},
	resolve: {
		extensions: ['', '.js']
	}
};